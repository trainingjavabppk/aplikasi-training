CREATE TABLE topik (
    id VARCHAR(36),
    kode VARCHAR(50) not null,
    nama VARCHAR(255) not null,
    durasi_jam INT not null,
    PRIMARY KEY (id),
    UNIQUE KEY (kode) 
);