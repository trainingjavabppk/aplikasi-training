package com.muhardin.endy.training.service;


import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;


@Service @Slf4j
public class FileUploadService {

    
    @Value("${upload.folder}")
    public String lokasiUpload;

    private Path lokasiHasilUpload;
    
    @PostConstruct
    public void inisialisasi() {
        log.debug("Inisialisasi file upload service");
        log.debug("Lokasi Upload : {}", lokasiUpload);

        this.lokasiHasilUpload = Paths.get(lokasiUpload)
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.lokasiHasilUpload);
            log.debug("Berhasil membuat direktori file Upload di : {} "
            ,lokasiHasilUpload.toAbsolutePath());
        } catch (Exception ex) {
            log.error("Gagal membuat lokasi Upload : {}"
            ,lokasiHasilUpload.toAbsolutePath());
        }
    }

    public String simpanFile(InputStream fileInputStream, String namaFile) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(namaFile);

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new IllegalArgumentException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.lokasiHasilUpload.resolve(fileName);
            Files.copy(fileInputStream, targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }


    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.lokasiHasilUpload.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new IllegalArgumentException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            log.error(ex.getMessage(), ex);
            return null;
        }
    }
}
