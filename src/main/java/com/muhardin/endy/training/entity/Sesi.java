package com.muhardin.endy.training.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.hibernate.annotations.GenericGenerator;
import lombok.Data;
import javax.persistence.Entity;


@Entity @Data
public class Sesi {

    @Id @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @JsonBackReference
    @ManyToOne @JoinColumn(name = "id_batch")
    private Batch batch;

    @ManyToOne @JoinColumn(name = "id_instruktur")
    private Instruktur instruktur;

    @JsonManagedReference
    @OneToMany(mappedBy = "sesi", cascade = CascadeType.ALL , orphanRemoval = true)
    private List<Presensi> daftarPresensi = new ArrayList<>();

    @NotNull
    private LocalDate tanggal;

    @NotNull
    private LocalTime jamMulai;
    
    @NotNull
    private LocalTime jamSelesai;
    
}
