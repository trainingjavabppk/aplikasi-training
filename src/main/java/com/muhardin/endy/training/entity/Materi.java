package com.muhardin.endy.training.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

import org.hibernate.annotations.GenericGenerator;
import lombok.Data;
import javax.persistence.Entity;


@Entity @Data
public class Materi {

    @Id @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_topik")
    private Topik topik;

    @NotNull @NotEmpty
    private String kode;

    @NotNull @NotEmpty
    private String nama;

    @NotNull @Min(1)
    private Integer durasiMenit;

    @JsonBackReference
    @ManyToMany(mappedBy = "daftarMateri")
    private List<Batch> daftarBatch = new ArrayList<>();

}
