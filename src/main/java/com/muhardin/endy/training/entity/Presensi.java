package com.muhardin.endy.training.entity;

import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

import org.hibernate.annotations.GenericGenerator;
import lombok.Data;
import javax.persistence.Entity;


@Entity @Data
public class Presensi {

    @Id @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @JsonBackReference
    @NotNull
    @ManyToOne @JoinColumn(name = "id_sesi")
    private Sesi sesi;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_peserta")
    private Peserta peserta;

    @NotNull
    private LocalTime jamMasuk;

    @NotNull
    private LocalTime jamKeluar;
    
}
