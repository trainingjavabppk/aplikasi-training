package com.muhardin.endy.training.entity;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.GenericGenerator;
import lombok.Data;

@Entity @Data
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = {"kode"})})
public class Topik {

    @Id @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(nullable = false, length = 50)
    private String kode;

    @Column(nullable = false, length = 255)
    private String nama;

     @Column(nullable = false)
    private BigDecimal harga;

}
