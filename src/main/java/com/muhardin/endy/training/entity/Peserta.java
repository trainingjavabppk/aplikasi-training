package com.muhardin.endy.training.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;
import javax.persistence.Entity;


@Entity @Data
public class Peserta {

    @Id @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty @Email @Size(min = 6, max = 100)
    private String email;

    @NotNull @NotEmpty @Size( min = 2, max = 255)
    private String nama;

    @NotNull @NotEmpty @Size(min = 2, max = 50)
    private String noHp;

    
    private String foto;
    
}
