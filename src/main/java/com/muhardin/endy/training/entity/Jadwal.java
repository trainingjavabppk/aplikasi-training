package com.muhardin.endy.training.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import lombok.Data;
import javax.persistence.Entity;


@Entity @Data
public class Jadwal {

    @Id @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne @JoinColumn(name = "id_topik")
    private Topik topik;

    @NotNull
    private LocalDate tanggalMulai;

    @NotNull
    private LocalDate tanggalSelesai;
    
}
