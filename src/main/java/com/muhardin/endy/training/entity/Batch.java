package com.muhardin.endy.training.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.hibernate.annotations.GenericGenerator;
import lombok.Data;


@Entity @Data
public class Batch {

    @Id @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_jadwal")
    private Jadwal jadwal;

    @JsonManagedReference
    @ManyToMany
    @JoinTable(name = "daftar_materi_batch", 
               joinColumns = @JoinColumn(name="id_batch"),
               inverseJoinColumns = @JoinColumn(name="id_materi")
        )
    private List<Materi> daftarMateri = new ArrayList<>();

    @ElementCollection
    @CollectionTable(
        name="daftar_tanggal_batch",
        joinColumns=@JoinColumn(name="id_batch"))
    @Column(name="tanggal_pelatihan")
    private List<LocalDate> daftarTanggalPelatihan = new ArrayList<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "batch", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Sesi> daftarSesi = new ArrayList<>();
    
    @NotNull
    private LocalDate tanggalMulai;

    @NotNull
    private LocalDate tanggalSelesai;
    
} 
