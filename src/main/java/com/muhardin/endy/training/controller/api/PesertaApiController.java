package com.muhardin.endy.training.controller.api;


import java.io.IOException;
import java.util.Optional;
import javax.validation.Valid;
import com.muhardin.endy.training.dao.PesertaDao;
import com.muhardin.endy.training.entity.Peserta;
import com.muhardin.endy.training.service.FileUploadService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;


@Controller
@RequestMapping("/api/peserta")
public class PesertaApiController {

    @Autowired private PesertaDao pesertaDao;  
    @Autowired private FileUploadService fileUploadService; 
    
    @GetMapping("/")
    @ResponseBody
    public Page<Peserta> dataPeserta (Pageable page) {
        return pesertaDao.findAll(page);        
    }

    @PostMapping("/")
    public ResponseEntity<Void> simpan(
            @RequestBody @Valid Peserta peserta, 
            UriComponentsBuilder uriBuilder ) {
                
         pesertaDao.save(peserta);
         UriComponents uriComponents = uriBuilder
                .path("api/peserta/{id}")
                .buildAndExpand(peserta.getId());
         return ResponseEntity.created(uriComponents.toUri()).build();
    }


    @PutMapping("/{id}/foto")
    @ResponseStatus(HttpStatus.OK)
    public void uploadFoto(@PathVariable("id") Peserta peserta,  
            @RequestParam("foto")
            MultipartFile fileFoto) throws IOException {
        
        String extension = getExtensionByStringHandling(fileFoto.getOriginalFilename())
            .orElse(fileFoto.getOriginalFilename());
        
        
        String namaFile = fileUploadService.simpanFile(fileFoto.getInputStream(), 
            "foto-" + peserta.getId() + "."+ extension);
         peserta.setFoto(namaFile);
         pesertaDao.save(peserta);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Peserta> pesertaById(@PathVariable("id")  String id) {
        Optional<Peserta> optPeserta = pesertaDao.findById(id);
        if(optPeserta.isPresent()) {
            return ResponseEntity.ok()
            .body(optPeserta.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(
        @PathVariable("id") Peserta pesertaDb,
        @RequestBody @Valid Peserta pesertaUpdate ) {
        
        if(pesertaDb == null) {
            return ResponseEntity.notFound().build();
        } else {
            pesertaUpdate.setId(pesertaDb.getId());
            BeanUtils.copyProperties(pesertaUpdate, pesertaDb);
            pesertaDao.save(pesertaDb);

            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(value = "id", required = false) Peserta peserta) {
        if (peserta != null) {
            pesertaDao.delete(peserta);
        }        
    }

    private Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
          .filter(f -> f.contains("."))
          .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

}
