package com.muhardin.endy.training.controller.api;

import com.muhardin.endy.training.dao.MateriDao;
import com.muhardin.endy.training.entity.Materi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/materi")
public class MateriApiController {
  
    @Autowired private MateriDao materiDao;

    @GetMapping("/")
    @ResponseBody
    public Page<Materi> dataMateri(Pageable page) {
        return materiDao.findAll(page);
    }

}
