package com.muhardin.endy.training.dao;

import com.muhardin.endy.training.entity.Jadwal;

import org.springframework.data.repository.CrudRepository;

public interface JadwalDao extends CrudRepository<Jadwal, String> {
    
}
