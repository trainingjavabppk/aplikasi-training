package com.muhardin.endy.training.dao;

import java.util.List;

import com.muhardin.endy.training.entity.Peserta;
import com.muhardin.endy.training.entity.Presensi;
import com.muhardin.endy.training.entity.Topik;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface PresensiDao extends PagingAndSortingRepository<Presensi, String> {

    List<Presensi> findBySesiBatchJadwalTopik(Topik t);

    @Query("SELECT DISTINCT(pr.peserta) from Presensi pr where pr.sesi.batch.jadwal.topik = :topik")
    List<Peserta> findPesertaForTopik(@Param(value = "topik") Topik t);

    @Query("SELECT DISTINCT(pr.peserta.email) from Presensi pr where pr.sesi.batch.jadwal.topik = :topik")
    List<String> findEmailPesertaForTopik(@Param(value = "topik") Topik t);
        
}
