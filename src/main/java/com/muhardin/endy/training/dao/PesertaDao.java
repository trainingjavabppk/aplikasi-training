package com.muhardin.endy.training.dao;

import java.util.List;

import com.muhardin.endy.training.entity.Peserta;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PesertaDao extends PagingAndSortingRepository<Peserta, String> {

    public List<Peserta> findByEmailContaining(String email);
    
}
