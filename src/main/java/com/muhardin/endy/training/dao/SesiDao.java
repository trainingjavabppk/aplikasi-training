package com.muhardin.endy.training.dao;

import com.muhardin.endy.training.entity.Sesi;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SesiDao extends PagingAndSortingRepository<Sesi, String> {
    
}
