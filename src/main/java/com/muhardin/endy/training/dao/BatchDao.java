package com.muhardin.endy.training.dao;

import com.muhardin.endy.training.entity.Batch;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BatchDao extends PagingAndSortingRepository<Batch, String> {

}
