package com.muhardin.endy.training.dao;

import java.util.List;

import com.muhardin.endy.training.entity.Materi;
import com.muhardin.endy.training.entity.Topik;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MateriDao extends PagingAndSortingRepository<Materi, String> {

    List<Materi> findByTopik(Topik t);
    
}
