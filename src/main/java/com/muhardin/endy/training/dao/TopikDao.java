package com.muhardin.endy.training.dao;

import com.muhardin.endy.training.entity.Topik;
import org.springframework.data.repository.CrudRepository;

public interface TopikDao extends CrudRepository<Topik, String> {
    
}
