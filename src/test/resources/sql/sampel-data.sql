INSERT INTO topik (id, harga, kode, nama) 
VALUES  ('J-001', 500000.00, 'JAV-FUND-001', 'Java Fundamental API');

INSERT INTO peserta (id, email, nama, no_hp)
VALUES ('P-001', 'peserta01@gmail.com', 'Zamu', '085719848475');

INSERT INTO peserta (id, email, nama, no_hp)
VALUES ('P-002', 'peserta02@gmail.com', 'Setiawan', '085719848476');

INSERT INTO peserta (id, email, nama, no_hp)
VALUES ('P-003', 'peserta03@gmail.com', 'Aji', '085719848477');

INSERT INTO instruktur (id, email, nama, no_hp)
VALUES ('I-001', 'instruktur01@gmail.com', 'Setiawan', '085719848475');

INSERT INTO jadwal (id, id_topik, tanggal_mulai, tanggal_selesai)
VALUES ('JD-001', 'J-001', '2021-02-01', '2021-02-05');

INSERT INTO materi (id, id_topik, kode, nama, durasi_menit )
VALUES ('M-001', 'J-001', 'MTR-001', 'Instalasi Java', 30);


INSERT INTO materi (id, id_topik, kode, nama, durasi_menit )
VALUES ('M-002', 'J-001', 'MTR-002', 'Hello World!', 45);


INSERT INTO materi (id, id_topik, kode, nama, durasi_menit )
VALUES ('M-003', 'J-001', 'MTR-003', 'Package dan Jar', 60); 

INSERT INTO batch (id, id_jadwal, tanggal_mulai, tanggal_selesai)
VALUES ('B-001', 'JD-001', '2021-03-01', '2021-03-04');

INSERT INTO daftar_tanggal_batch (id_batch, tanggal_pelatihan)
VALUES ('B-001', '2021-03-01');

INSERT INTO daftar_tanggal_batch (id_batch, tanggal_pelatihan)
VALUES ('B-001', '2021-03-02');

INSERT INTO daftar_tanggal_batch (id_batch, tanggal_pelatihan)
VALUES ('B-001', '2021-03-03');

INSERT INTO daftar_tanggal_batch (id_batch, tanggal_pelatihan)
VALUES ('B-001', '2021-03-04');

INSERT INTO daftar_materi_batch (id_batch, id_materi)
VALUES ('B-001', 'M-001');

INSERT INTO daftar_materi_batch (id_batch, id_materi)
VALUES ('B-001', 'M-002');

INSERT INTO sesi (id, id_batch, id_instruktur, tanggal, jam_mulai, jam_selesai)
VALUES ('S-001', 'B-001', 'I-001', '2021-03-01', '08:00:01', '16:15:00');

INSERT INTO sesi (id, id_batch, id_instruktur, tanggal, jam_mulai, jam_selesai)
VALUES ('S-002', 'B-001', 'I-001', '2021-03-03', '08:30:01', '16:00:00');

INSERT INTO presensi (id, id_sesi, id_peserta, jam_masuk, jam_keluar)
VALUES ('PR-101', 'S-001', 'P-001', '08:05:01', '16:15:00');

INSERT INTO presensi (id, id_sesi, id_peserta, jam_masuk, jam_keluar)
VALUES ('PR-102', 'S-001', 'P-002', '08:00:00', '16:15:00');

INSERT INTO presensi (id, id_sesi, id_peserta, jam_masuk, jam_keluar)
VALUES ('PR-201', 'S-002', 'P-001', '08:15:01', '16:00:00');

INSERT INTO presensi (id, id_sesi, id_peserta, jam_masuk, jam_keluar)
VALUES ('PR-202', 'S-002', 'P-003', '08:15:05', '16:10:00');