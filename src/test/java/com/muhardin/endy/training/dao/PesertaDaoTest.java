package com.muhardin.endy.training.dao;

import java.util.List;
import com.muhardin.endy.training.entity.Peserta;
import com.muhardin.endy.training.entity.Presensi;
import com.muhardin.endy.training.entity.Topik;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = {
     "classpath:/sql/reset-data.sql",
     "classpath:/sql/sampel-data.sql"
})

public class PesertaDaoTest {

    @Autowired private PesertaDao pesertaDao;
    @Autowired private PresensiDao presensiDao;

    @Test
    public void testCariPesertaYangTelahIkutTopikTertentu() {

        Topik t = new Topik();
        t.setId("J-001");

        List<Presensi> daftarPresensiSemuaTrainingJavaFundamental 
            = presensiDao.findBySesiBatchJadwalTopik(t);
        
        Assertions.assertEquals(4, daftarPresensiSemuaTrainingJavaFundamental.size());

        System.out.println("==========================================================");
        
        List<Peserta> daftarPesertaJavaFundamental 
            = presensiDao.findPesertaForTopik(t);
        
        Assertions.assertEquals(3, daftarPesertaJavaFundamental.size());

        for(Peserta p : daftarPesertaJavaFundamental) {
            System.out.println("Nama peserta : " + p.getNama());
        }

        List<String> daftarEmailPesertaTrainingJavaFundamental 
            = presensiDao.findEmailPesertaForTopik(t);
        
        Assertions.assertEquals(3, daftarEmailPesertaTrainingJavaFundamental.size());;

        for(String email : daftarEmailPesertaTrainingJavaFundamental) {
            System.out.println("Email Peserta :" + email );
        }
    }
    
    @Test
    public void testCariPesertaByEmail() {

        List<Peserta> daftarPesertaByEmail = pesertaDao.findByEmailContaining("@gmail.com");

        Assertions.assertEquals(3, daftarPesertaByEmail.size());;

        for(Peserta p : daftarPesertaByEmail) {
            System.out.println("Peserta dengan email " + p.getEmail() + " adalah " + p.getNama());
        }

    }
    
}
