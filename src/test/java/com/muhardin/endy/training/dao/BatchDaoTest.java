package com.muhardin.endy.training.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import com.muhardin.endy.training.entity.Batch;
import com.muhardin.endy.training.entity.Jadwal;
import com.muhardin.endy.training.entity.Materi;
import com.muhardin.endy.training.entity.Topik;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = {
     "classpath:/sql/reset-data.sql",
     "classpath:/sql/sampel-data.sql"
})
    
    public  class BatchDaoTest {

    private static final String SQL_HITUNG_TANGGAL_BATCH = "SELECT COUNT(*) FROM daftar_tanggal_batch WHERE id_batch = ?";
    private static final String SQL_HITUNG_MATERI_BATCH = "SELECT COUNT(*) FROM daftar_materi_batch WHERE id_batch = ?";
    
    @Autowired private BatchDao batchDao;
    @Autowired private JadwalDao jadwalDao;
    @Autowired private MateriDao materiDao;

    @Autowired private DataSource dataSource;

    @Test
    public void testCreateBatch() {
        Optional<Jadwal> optJadwal = jadwalDao.findById("JD-001");
        Assertions.assertTrue(optJadwal.isPresent());

        Jadwal j = optJadwal.get();
    
        Batch b = new Batch();
        b.setJadwal(j);
        b.setTanggalMulai(j.getTanggalMulai());
        b.setTanggalSelesai(j.getTanggalSelesai());

        LocalDate tanggal = j.getTanggalMulai();
        while (tanggal.isBefore(j.getTanggalSelesai())) {
            b.getDaftarTanggalPelatihan().add(tanggal);
            tanggal = tanggal.plusDays(1);
        }

        b.getDaftarTanggalPelatihan().add(tanggal);

        Assertions.assertEquals(5, b.getDaftarTanggalPelatihan().size());

        Topik t = new Topik();
        t.setId("J-001");

        List<Materi> daftarMateri = materiDao.findByTopik(t);
        Assertions.assertFalse(daftarMateri.isEmpty());

        b.setDaftarMateri(daftarMateri);
        batchDao.save(b);
        Assertions.assertNotNull(b.getId());
        
    }

    @Test
    public void testDeleteBatch() throws SQLException {
        String idBatch = "B-001";
        Optional<Batch> b = batchDao.findById(idBatch);
        Assertions.assertTrue(b.isPresent());

        batchDao.delete(b.get());

        PreparedStatement psMateri = dataSource.getConnection().prepareStatement(SQL_HITUNG_MATERI_BATCH);
        psMateri.setString(1, idBatch);

        ResultSet rsMateri = psMateri.executeQuery();
        Assertions.assertTrue(rsMateri.next());
        Assertions.assertEquals(0, rsMateri.getLong(1));

        PreparedStatement psTanggal = dataSource.getConnection().prepareStatement(SQL_HITUNG_TANGGAL_BATCH);
        psTanggal.setString(1, idBatch);

        ResultSet rsTanggal = psTanggal.executeQuery();
        Assertions.assertTrue(rsTanggal.next());
        Assertions.assertEquals(0, rsTanggal.getLong(1));
    } 

}
