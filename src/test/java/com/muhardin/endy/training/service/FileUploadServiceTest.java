package com.muhardin.endy.training.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;

@SpringBootTest
public class FileUploadServiceTest {

    @Autowired private FileUploadService fileUploadService;

    @Value("classpath:file/silabus-training.txt")
    private Resource contohFile;

    @Test
    public void testSimpanFile() throws IOException {
        String hasil = 
        fileUploadService.simpanFile(contohFile.getInputStream(), "test-upload.txt");
        Assertions.assertNotNull(hasil);
        System.out.println("File yang disimpan :" + hasil);


        Resource hasilUpload = fileUploadService.loadFileAsResource(hasil);
        String text = new BufferedReader(new InputStreamReader(hasilUpload.getInputStream(),
             StandardCharsets.UTF_8))
        .lines()
        .collect(Collectors.joining("\n"));

        System.out.println("Isi File : ");
        System.out.println("=================================================");
        System.out.println(text);
        System.out.println("=================================================");
    }    
}
